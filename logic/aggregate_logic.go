package logic

import (
	"context"
	"mediarequests/entities"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/valyala/fasthttp"
	"gitlab.wikimedia.org/frankie/aqsassist"
	"schneider.vip/problem"
)

type AggregateLogic struct {
}

func (s *AggregateLogic) ProcessAggregateLogic(context context.Context, ctx *fasthttp.RequestCtx, referer, mediaType, agent, granularity, start, end string, session *gocql.Session, rLogger *logger.Logger) (*problem.Problem, entities.AggregateResponse, int) {
	var err error
	var problemData *problem.Problem
	var response = entities.AggregateResponse{Items: make([]entities.Aggregate, 0)}

	query := `SELECT requests, timestamp FROM "local_group_default_T_mediarequest_per_referer".data WHERE "_domain" = 'analytics.wikimedia.org' AND referer = ? AND media_type = ? AND agent = ? AND granularity = ? AND timestamp >= ? AND timestamp <= ?`
	scanner := session.Query(query, referer, mediaType, agent, granularity, start, end).WithContext(ctx).Iter().Scanner()
	var requests int
	var timestamp string

	for scanner.Next() {
		if err = scanner.Scan(&requests, &timestamp); err != nil {
			rLogger.Log(logger.ERROR, "Query failed: %s", err)
			problemData = aqsassist.CreateProblem(http.StatusInternalServerError, err.Error(), string(ctx.Request.URI().RequestURI()))
			return problemData, entities.AggregateResponse{}, http.StatusInternalServerError

		}
		response.Items = append(response.Items, entities.Aggregate{
			Referer:     referer,
			MediaType:   mediaType,
			Agent:       agent,
			Granularity: granularity,
			Timestamp:   timestamp,
			Requests:    requests,
		})
	}

	if err := scanner.Err(); err != nil {
		rLogger.Error("Error querying database: %s", err)
		problemData = aqsassist.CreateProblem(http.StatusInternalServerError, err.Error(), string(ctx.Request.URI().RequestURI()))
		return problemData, entities.AggregateResponse{}, http.StatusInternalServerError
	}

	return problemData, response, 0
}
