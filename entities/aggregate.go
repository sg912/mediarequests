package entities

// AggregateResponse represents the API resultset	.
type AggregateResponse struct {
	Items []Aggregate `json:"items"`
}

// Aggregate represents an entry in the API resultset.
type Aggregate struct {
	Referer     string `json:"referer"`
	MediaType   string `json:"media_type"`
	Agent       string `json:"agent"`
	Granularity string `json:"granularity"`
	Timestamp   string `json:"timestamp"`
	Requests    int    `json:"requests"`
}
