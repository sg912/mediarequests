package entities

// TopResponse represents the API resultset	.
type TopResponse struct {
	Items []Top `json:"items"`
}

// Top represents an entry in the API resultset.
type Top struct {
	Referer   string     `json:"referer"`
	MediaType string     `json:"media_type"`
	Year      string     `json:"year"`
	Month     string     `json:"month"`
	Day       string     `json:"day"`
	Files     []TopFiles `json:"top"`
}

// TopFiles represents the set of files returned within the API resultset.
type TopFiles struct {
	FilePath string `json:"file_path"`
	Requests int    `json:"requests"`
	Rank     int    `json:"rank"`
}
